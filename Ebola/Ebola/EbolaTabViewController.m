//
//  EbolaTabViewController.m
//  Ebola
//
//  Created by Administrator on 8/25/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import "EbolaTabViewController.h"

@interface EbolaTabViewController ()

@end

@implementation EbolaTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    UIImage *tabBackground = [[UIImage imageNamed:@"tab_bg"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UITabBar appearance] setBackgroundImage:tabBackground];
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0f],
                                                        NSForegroundColorAttributeName : [UIColor whiteColor]
                                                        } forState:UIControlStateSelected];
    
    UIImage *selectedImage = [UIImage imageNamed:@"ebola_tab1"];
    UIImage *unselectedImage = [UIImage imageNamed:@"tab1a"];
    UITabBar *tabBar = (UITabBar*)self.tabBar;
    
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"CustomUITabbarSelected.png"]];
    
    UITabBarItem *item1 = [tabBar.items objectAtIndex:0];
    [item1 setImage:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item1 setSelectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    selectedImage = [UIImage imageNamed:@"ebola_tab2"];
    unselectedImage = [UIImage imageNamed:@"tab2a"];
    UITabBarItem *item2 = [tabBar.items objectAtIndex:1];
    [item2 setImage:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item2 setSelectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    selectedImage = [UIImage imageNamed:@"ebola_tab3"];
    unselectedImage = [UIImage imageNamed:@"tab3a"];
    UITabBarItem *item3 = [tabBar.items objectAtIndex:2];
    [item3 setImage:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item3 setSelectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    selectedImage = [UIImage imageNamed:@"ebola_tab4"];
    unselectedImage = [UIImage imageNamed:@"tab4a"];
    UITabBarItem *item4 = [tabBar.items objectAtIndex:3];
    [item4 setImage:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item4 setSelectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

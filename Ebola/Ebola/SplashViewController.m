//
//  SplashViewController.m
//  Ebola
//
//  Created by Administrator on 8/26/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import "SplashViewController.h"
#import "EbolaCase.h"
#import "Styles.h"
#import "EbolaServerManager.h"

@interface SplashViewController ()
@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishedDataLoad:)
                                                 name:@"DataLoad"
                                               object:nil];
    
    EbolaServerManager *sharedManager = [EbolaServerManager sharedManager];
    [sharedManager loadXML];
    
    
    [loader startAnimating];
}

-(void)finishedDataLoad:(id)sender {
    [self performSegueWithIdentifier:@"startApp" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  EbolaServerManager.h
//  Ebola
//
//  Created by Administrator on 10/14/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EbolaCase.h"
#import <MapKit/MapKit.h>
#import "Styles.h"

@interface EbolaServerManager : NSObject {
    
}

+ (id)sharedManager;

- (void)loadXML;

@end
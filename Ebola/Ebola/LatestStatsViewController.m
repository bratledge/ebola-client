//
//  LatestStatsViewController.m
//  Ebola
//
//  Created by Administrator on 8/29/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import "LatestStatsViewController.h"
#import "EbolaCase.h"
#import "EbolaServerManager.h"

@interface LatestStatsViewController ()

@end

@implementation LatestStatsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    refreshIndicator.hidden = YES;
    [refreshIndicator stopAnimating];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification_OrientationDidChange:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishedDataLoad:)
                                                 name:@"DataLoad"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishedDataLoadFailed:)
                                                 name:@"DataLoadFailed"
                                               object:nil];
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.1];
    
}

-(void)notification_OrientationDidChange:(NSNotification*)n
{
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.1];
    //[self loadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.1];
}

- (void)loadData {
    
    numberCasesChart.text = @"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSString *theDate = [dateFormat stringFromDate:now];
    lastUpdated.text = [NSString stringWithFormat:@"Last updated: %@", theDate];
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    float os_version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (os_version >= 8.000000)
    {
        //Use AlertController
        [locationManager requestWhenInUseAuthorization];
    }
    else
    {
        //UIAlertView
    }
    
    
    [locationManager startUpdatingLocation];
    
    AppDelegate *d = [[UIApplication sharedApplication] delegate];
    
    suspConfCases.text = d.suspectedConfirmedCases;
    suspConfDeath.text = d.suspectedCaseDeaths;
    confirmedCases.text = d.confirmedLabCases;
    
    latestCases.text = d.latestCases;
    
    customMessage.text = d.customMessage;
    vaccineStatus.text = d.vaccineMessage;
    
    if ([d.vaccineColor isEqualToString:@"1"])
        vaccineStatus.backgroundColor = [UIColor greenColor];
    else if ([d.vaccineColor isEqualToString:@"2"])
        vaccineStatus.backgroundColor = [UIColor orangeColor];
    else if ([d.vaccineColor isEqualToString:@"3"])
        vaccineStatus.backgroundColor = [UIColor redColor];
    
    if (lineChartView)
        [lineChartView removeFromSuperview];
    
    thisChartView.frame = CGRectMake(thisChartView.frame.origin.x, thisChartView.frame.origin.y, scrollView.frame.size.width, thisChartView.frame.size.height);
    
    lineChartView = [[JBLineChartView alloc] init];
    lineChartView.dataSource = self;
    lineChartView.delegate = self;
    
    lineChartView.frame = CGRectMake(0,0,scrollView.frame.size.width,190);
    
    lineChartView.backgroundColor = [UIColor darkGrayColor];
    [lineChartView setMaximumValue:20000];
    [lineChartView setMinimumValue:0];
    
    [thisChartView addSubview:lineChartView];
    [lineChartView reloadData];
}

#pragma mark - JB Line Chart Delegate methods

- (NSUInteger)numberOfLinesInLineChartView:(JBLineChartView *)lineChartView
{
    return 1; // number of lines in chart
}

- (NSUInteger)lineChartView:(JBLineChartView *)lineChartView numberOfVerticalValuesAtLineIndex:(NSUInteger)lineIndex
{
    AppDelegate *d = [[UIApplication sharedApplication] delegate];
    return [d.dailyCases count]; // number of values for a line
}

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView verticalValueForHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex
{
    AppDelegate *d = [[UIApplication sharedApplication] delegate];
    CGFloat myFloatVal = [[d.dailyCases objectAtIndex:horizontalIndex] intValue] * 1.0;
    return myFloatVal;
}




- (UIColor *)lineChartView:(JBLineChartView *)lineChartView colorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return [UIColor whiteColor]; // color of line in chart
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView fillColorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return [UIColor whiteColor]; // color of area under line in chart
}

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView widthForLineAtLineIndex:(NSUInteger)lineIndex
{
    return 1.0; // width of line in chart
}

- (JBLineChartViewLineStyle)lineChartView:(JBLineChartView *)lineChartView lineStyleForLineAtLineIndex:(NSUInteger)lineIndex
{
    return JBLineChartViewLineStyleSolid; // style of line in chart
}

- (void)lineChartView:(JBLineChartView *)lineChartView didSelectLineAtIndex:(NSUInteger)lineIndex horizontalIndex:(NSUInteger)horizontalIndex touchPoint:(CGPoint)touchPoint
{
    // Update view
    AppDelegate *d = [[UIApplication sharedApplication] delegate];
    numberCasesChart.text = [NSString stringWithFormat:@"%@",[d.dailyCases objectAtIndex:horizontalIndex]];
}

- (void)didDeselectLineInLineChartView:(JBLineChartView *)lineChartView
{
    // Update view
    numberCasesChart.text = @"";
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    
    double latA = newLocation.coordinate.latitude;
    double logA = newLocation.coordinate.longitude;
    
    AppDelegate *d = [[UIApplication sharedApplication] delegate];
    float originalDistance = 100000;
    for (EbolaCase *e in d.ebolaCases) {
        double latB = e.latitude;
        double logB = e.longitude;
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:latA
                                                      longitude:logA];
        
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:latB longitude:logB];
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        if (distance/1609.344 < originalDistance)
            originalDistance = distance/1609.344;
        
    }

    distanceInMiles.text = [NSString stringWithFormat:@"%.1f miles",originalDistance];
    
    [locationManager stopUpdatingLocation];
    
    
    //NSLog(@"Calculated Miles %@", [NSString stringWithFormat:@"%.1fmi",(distance/1609.344)]);
}

-(IBAction)reloadData:(id)sender {
    refreshIndicator.hidden = NO;
    [refreshIndicator startAnimating];
    
    //check for > 1 min passed to reload data
    NSDate *lastUsedDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastRefreshedApp"];
    if (lastUsedDate != nil)
    {
        NSDate* timeNow = [NSDate date];
        
        if ([timeNow timeIntervalSinceDate:lastUsedDate] > 300.0)
        {
            
            EbolaServerManager *sharedManager = [EbolaServerManager sharedManager];
            [sharedManager loadXML];
        }
        else
        {
            [[[UIAlertView alloc]initWithTitle:@"No New Data" message:@"Standby for new updates" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            refreshIndicator.hidden = YES;
            [refreshIndicator stopAnimating];
        }
    } else {
        EbolaServerManager *sharedManager = [EbolaServerManager sharedManager];
        [sharedManager loadXML];
    }
    
    
    

}

-(void)finishedDataLoad:(id)sender {
    [self loadData];
    refreshIndicator.hidden = YES;
    [refreshIndicator stopAnimating];
}

-(void)finishedDataLoadFailed:(id)sender {
    [[[UIAlertView alloc]initWithTitle:@"Network failure" message:@"Failed to get latest outbreak data." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    refreshIndicator.hidden = YES;
    [refreshIndicator stopAnimating];
}


@end

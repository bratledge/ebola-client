//
//  LatestStatsViewController.h
//  Ebola
//
//  Created by Administrator on 8/29/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "JBChartView/JBChartView.h"
#import "JBChartView/JBBarChartView.h"
#import "JBChartView/JBLineChartView.h"

@interface LatestStatsViewController : UIViewController <CLLocationManagerDelegate,JBLineChartViewDataSource,JBLineChartViewDelegate> {
    
    CLLocationManager *locationManager;
    
    IBOutlet UILabel *lastUpdated;
    
    IBOutlet UITextField *distanceInMiles;
    
    IBOutlet UITextField *suspConfCases;
    IBOutlet UITextField *suspConfDeath;
    
    IBOutlet UITextField *confirmedCases;
    
    IBOutlet UIView *thisChartView;
    
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UILabel *customMessage;
    IBOutlet UITextField *vaccineStatus;
    
    IBOutlet UITextField *latestCases;
    
    IBOutlet UIActivityIndicatorView *refreshIndicator;
    
    IBOutlet UILabel *numberCasesChart;
    
    JBLineChartView *lineChartView;
}


@end

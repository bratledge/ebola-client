//
//  Styles.h
//  Ebola
//
//  Created by Administrator on 8/27/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Styles : NSObject

@property (strong, nonatomic) NSString *styleId;
@property (strong, nonatomic) NSString *icon;

@end

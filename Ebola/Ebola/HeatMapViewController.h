//
//  HeatMapViewController.h
//  Ebola
//
//  Created by Administrator on 8/26/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "HeatMap.h"
#import "HeatMapView.h"

@interface HeatMapViewController : UIViewController <MKMapViewDelegate>
{
    HeatMap *hm;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UILabel *date;

@end

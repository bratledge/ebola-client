//
//  AboutEbolaViewController.m
//  Ebola
//
//  Created by Administrator on 8/29/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import "AboutEbolaViewController.h"

@interface AboutEbolaViewController ()

@end

@implementation AboutEbolaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)symptomsSelected:(id)sender {
    
}

- (IBAction)transmissionSelected:(id)sender {
    
}

- (IBAction)exposureSelected:(id)sender {
    
}

- (IBAction)preventionSelected:(id)sender {
    
}

- (IBAction)diagnosisSelected:(id)sender {
    
}
@end

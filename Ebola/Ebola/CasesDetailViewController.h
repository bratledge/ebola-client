//
//  CasesDetailViewController.h
//  Ebola
//
//  Created by Administrator on 9/15/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EbolaCase.h"

@interface CasesDetailViewController : UIViewController <UIWebViewDelegate> {
    IBOutlet UIWebView *caseWebView;
}

@property (nonatomic,strong) EbolaCase *thisCase;

@end

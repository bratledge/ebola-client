//
//  HeatMap.h
//  HeatMap

#import <MapKit/MapKit.h>

@interface HeatMap : NSObject <MKOverlay>

//For the heatMapData dictionaries:
//keys need to be NSValues encoded with MKMapPoints
//vaules need to be NSNumbers representing the relative heat for the point
//values should be positive

- (id)initWithData:(NSDictionary *)heatMapData;

- (void)setData:(NSDictionary *)newHeatMapData;

- (NSDictionary *)mapPointsWithHeatInMapRect:(MKMapRect)rect atScale:(MKZoomScale)scale;

- (MKMapRect)boundingMapRect;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@end

//
//  EbolaCase.h
//  Ebola
//
//  Created by Administrator on 8/27/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EbolaCase : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *caseDescription;
@property (strong, nonatomic) NSString *style;  //remove the # symbol
@property (strong, nonatomic) NSString *coordinates;
@property double latitude;
@property double longitude;
@property (strong, nonatomic) NSDate *date;

@end

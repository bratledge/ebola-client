//
//  EbolaServerManager.m
//  Ebola
//
//  Created by Administrator on 10/14/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import "EbolaServerManager.h"

@implementation EbolaServerManager

#pragma mark Singleton Methods

+ (id)sharedManager {
    static EbolaServerManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)loadXML {
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:@"text/xml" forHTTPHeaderField:@"Accept"];
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/vnd.google-earth.kml+xml"];
    NSDictionary *parameters = @{};
    NSString *postURL = [NSString stringWithFormat:@"%@",ROOT_URL,nil];
    [manager POST:postURL
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSData * data = (NSData *)responseObject;
              NSString *fetchedXML = [NSString stringWithCString:[data bytes] encoding:NSISOLatin1StringEncoding];
              NSLog(@"Response string: %@", fetchedXML);
              
              [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastRefreshedApp"];
              [[NSUserDefaults standardUserDefaults] synchronize];
              
              if (fetchedXML)
              {
                  if (![fetchedXML isEqualToString:@""])
                  {
                      //  1) parse the data
                      NSError *error;
                      TBXML * tbxml = [TBXML newTBXMLWithXMLString:fetchedXML error:&error];
                      TBXMLElement * root = tbxml.rootXMLElement;
                      TBXMLElement * document = [TBXML childElementNamed:@"Document" parentElement:root];
                      
                      AppDelegate *d = [[UIApplication sharedApplication] delegate];
                      
                      d.styles = [[NSMutableArray alloc] initWithCapacity:5000];
                      
                      TBXMLElement * suspectedConfirmedCases = [TBXML childElementNamed:@"SuspectedConfirmedCases" parentElement:document];
                      d.suspectedConfirmedCases = [[NSString alloc] initWithCString:suspectedConfirmedCases->text encoding:NSUTF8StringEncoding];
                      
                      TBXMLElement * suspectedCaseDeaths = [TBXML childElementNamed:@"SuspectedCaseDeaths" parentElement:document];
                      d.suspectedCaseDeaths = [[NSString alloc] initWithCString:suspectedCaseDeaths->text encoding:NSUTF8StringEncoding];
                      
                      TBXMLElement * confirmedLabCases = [TBXML childElementNamed:@"ConfirmedLabCases" parentElement:document];
                      d.confirmedLabCases = [[NSString alloc] initWithCString:confirmedLabCases->text encoding:NSUTF8StringEncoding];
                      
                      TBXMLElement * vaccineColor = [TBXML childElementNamed:@"VaccineColor" parentElement:document];
                      d.vaccineColor = [[NSString alloc] initWithCString:vaccineColor->text encoding:NSUTF8StringEncoding];
                      
                      TBXMLElement * vaccineStatus = [TBXML childElementNamed:@"VaccineStatus" parentElement:document];
                      d.vaccineMessage = [[NSString alloc] initWithCString:vaccineStatus->text encoding:NSUTF8StringEncoding];
                      
                      TBXMLElement * customMessage = [TBXML childElementNamed:@"Homemsg" parentElement:document];
                      d.customMessage = [[NSString alloc] initWithCString:customMessage->text encoding:NSUTF8StringEncoding];
                      
                      TBXMLElement * latestCases = [TBXML childElementNamed:@"LatestCases" parentElement:document];
                      d.latestCases = [[NSString alloc] initWithCString:latestCases->text encoding:NSUTF8StringEncoding];
                       
                      //TBXMLElement * deathCount = [TBXML childElementNamed:@"deathcount" parentElement:document];
                      
                      TBXMLElement * caseCount = [TBXML childElementNamed:@"casecount" parentElement:document];
                      TBXMLElement * caseCountSibling = [TBXML childElementNamed:@"case" parentElement:caseCount];
                      
                      d.dailyCases = [[NSMutableArray alloc] initWithCapacity:5000];
                      d.dailyDates = [[NSMutableArray alloc] initWithCapacity:5000];
                      
                      do {
                          
                          TBXMLElement * dateValue = [TBXML childElementNamed:@"date" parentElement:caseCountSibling];
                          TBXMLElement * caseCountValue = [TBXML childElementNamed:@"count" parentElement:caseCountSibling];
                          
                          if (dateValue)
                          {
                              //parse date
                              
                              NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                              [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                              NSDate *dateFromString = [[NSDate alloc] init];
                              dateFromString = [dateFormatter dateFromString:[[NSString alloc] initWithCString:dateValue->text encoding:NSUTF8StringEncoding]];
                              
                              if (dateFromString == nil)
                              {
                                  [dateFormatter setDateFormat:@"M/dd/yyyy"];
                                  dateFromString = [[NSDate alloc] init];
                                  dateFromString = [dateFormatter dateFromString:[[NSString alloc] initWithCString:dateValue->text encoding:NSUTF8StringEncoding]];
                              }
                              
                              if (caseCountValue)
                              {
                                  NSString *caseStr = [[NSString alloc] initWithCString:caseCountValue->text encoding:NSUTF8StringEncoding];
                                  //add date to date array
                                  [d.dailyDates addObject:dateFromString];
                                  //add case count to case array
                                  [d.dailyCases addObject:caseStr];
                              }
                          }
                          
                          
                      } while ((caseCountSibling = caseCountSibling->nextSibling));
                      /*
                       
                       TBXMLElement * stylesElement = [TBXML childElementNamed:@"Style" parentElement:document];
                       
                       
                       do {
                       Styles *styleObj = [[Styles alloc] init];
                       TBXMLElement * iconStyle = [TBXML childElementNamed:@"IconStyle" parentElement:stylesElement];
                       if (iconStyle)
                       {
                       TBXMLElement * icon = [TBXML childElementNamed:@"Icon" parentElement:iconStyle];
                       TBXMLElement * iconHref = [TBXML childElementNamed:@"href" parentElement:icon];
                       
                       NSString *styleUrlText = [[NSString alloc] initWithCString:iconHref->text encoding:NSUTF8StringEncoding];
                       //caution
                       
                       TBXMLAttribute * attribute = stylesElement->firstAttribute;
                       NSString *styleIdText = [[NSString alloc] initWithCString:attribute->value encoding:NSUTF8StringEncoding];
                       
                       styleObj.styleId = styleIdText;
                       styleObj.icon = styleUrlText;
                       
                       [d.styles addObject:styleObj];
                       } else {
                       break;
                       }
                       
                       } while ((stylesElement = stylesElement->nextSibling));
                       
                       */
                      
                      
                      TBXMLElement * placemark = [TBXML childElementNamed:@"Placemark" parentElement:document];
                      
                      NSMutableDictionary *placeMarkLocations = [[NSMutableDictionary alloc] initWithCapacity:5000];
                      d.ebolaCases = [[NSMutableArray alloc] initWithCapacity:5000];
                      
                      
                      do {
                          // Display the name of the element
                          //NSLog(@"%@",[TBXML elementName:placemark]);
                          
                          // Obtain first attribute from element
                          //TBXMLAttribute * attribute = placemark->firstAttribute;
                          
                          EbolaCase *ebolaCase = [[EbolaCase alloc]init];
                          
                          TBXMLElement * point = [TBXML childElementNamed:@"Point" parentElement:placemark];
                          TBXMLElement * coordinates = [TBXML childElementNamed:@"coordinates" parentElement:point];
                          
                          NSString *coordinateText = [[NSString alloc] initWithCString:coordinates->text encoding:NSUTF8StringEncoding];
                          [coordinateText stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                          NSArray *items = [coordinateText componentsSeparatedByString:@","];
                          
                          
                          MKMapPoint mapPoint = MKMapPointForCoordinate(
                                                                        CLLocationCoordinate2DMake([[items objectAtIndex:1] doubleValue],
                                                                                                   [[items objectAtIndex:0] doubleValue]));
                          
                          NSValue *pointValue = [NSValue value:&mapPoint withObjCType:@encode(MKMapPoint)];
                          
                          
                          
                          ebolaCase.coordinates = coordinateText;
                          ebolaCase.latitude = [[items objectAtIndex:1] doubleValue];
                          ebolaCase.longitude = [[items objectAtIndex:0] doubleValue];
                          
                          TBXMLElement * date = [TBXML childElementNamed:@"date" parentElement:placemark];
                          if (date)
                          {
                              NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                              [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                              NSDate *dateFromString = [[NSDate alloc] init];
                              dateFromString = [dateFormatter dateFromString:[[NSString alloc] initWithCString:date->text encoding:NSUTF8StringEncoding]];
                              
                              if (dateFromString == nil)
                              {
                                  [dateFormatter setDateFormat:@"M/dd/yyyy"];
                                  dateFromString = [[NSDate alloc] init];
                                  dateFromString = [dateFormatter dateFromString:[[NSString alloc] initWithCString:date->text encoding:NSUTF8StringEncoding]];
                              }
                              
                              ebolaCase.date = dateFromString;
                              if (dateFromString)
                              {
                                  NSDateComponents *comps = [[NSDateComponents alloc] init];
                                  [comps setDay:1];
                                  [comps setMonth:1];
                                  [comps setYear:1955];
                                  NSDate *thisYear = [[NSCalendar currentCalendar] dateFromComponents:comps];
                                  
                                  if ([ebolaCase.date compare:thisYear] == NSOrderedDescending) {
                                      NSLog(@"date1 is later than date2");
                                      [placeMarkLocations setObject:[NSNumber numberWithInt:1] forKey:pointValue];
                                  }
                              }
                          }
                          
                          TBXMLElement *name = [TBXML childElementNamed:@"name" parentElement:placemark];
                          //TBXMLElement *styleUrl = [TBXML childElementNamed:@"styleUrl" parentElement:placemark];
                          
                          ebolaCase.name = [[NSString alloc] initWithCString:name->text encoding:NSUTF8StringEncoding];
                          //ebolaCase.style = [[[NSString alloc] initWithCString:styleUrl->text encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                          
                          TBXMLElement *description = [TBXML childElementNamed:@"description" parentElement:placemark];
                          if (description != nil)
                              ebolaCase.caseDescription  = [TBXML textForElement:description];//[[NSString alloc] initWithCString:description->text encoding:NSUTF8StringEncoding];
                          
                          [d.ebolaCases addObject:ebolaCase];
                          
                          /*
                           // if attribute is valid
                           while (attribute) {
                           // Display name and value of attribute to the log window
                           NSLog(@"%@->%@ = %@",
                           [TBXML elementName:element],
                           [TBXML attributeName:attribute],
                           [TBXML attributeValue:attribute]);
                           
                           // Obtain the next attribute
                           attribute = attribute->next;
                           }
                           */
                          
                          // Obtain next sibling element
                      } while ((placemark = placemark->nextSibling));
                      
                      
                      d.coordinates = placeMarkLocations;
                      
                      //  2) set timer for 1 hour to download again
                      
                  }
              }
              
              [[NSNotificationCenter defaultCenter] postNotificationName:@"DataLoad" object:self];
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [[[UIAlertView alloc] initWithTitle:@"Error loading data" message:@"There was a problem loading outbreak data" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
              [[NSNotificationCenter defaultCenter] postNotificationName:@"DataLoadFailed" object:self];
          }
     ];
    
    
    
}

- (void)dealloc {
    
}
@end

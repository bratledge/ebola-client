//
//  CasesDetailViewController.m
//  Ebola
//
//  Created by Administrator on 9/15/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import "CasesDetailViewController.h"

@interface CasesDetailViewController ()

@end

@implementation CasesDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    caseWebView.scalesPageToFit = YES;
    if ([self.thisCase.caseDescription hasPrefix:@"<"])
    {
        [caseWebView loadHTMLString:self.thisCase.caseDescription baseURL:nil];
    }
    else
    {
        NSURL *url = [NSURL URLWithString:self.thisCase.caseDescription];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [caseWebView loadRequest:requestObj];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

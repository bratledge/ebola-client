//
//  SplashViewController.h
//  Ebola
//
//  Created by Administrator on 8/26/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface SplashViewController : UIViewController {
    IBOutlet UIActivityIndicatorView *loader;
}

@end
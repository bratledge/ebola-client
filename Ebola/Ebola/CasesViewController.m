//
//  CasesViewController.m
//  Ebola
//
//  Created by Administrator on 9/9/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import "CasesViewController.h"
#import "IndividualCasesCell.h"
#import "EbolaCase.h"
#import "CasesDetailViewController.h"

@interface CasesViewController ()

@end

@implementation CasesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AppDelegate *d = [UIApplication sharedApplication].delegate;
    if (d.ebolaCases)
        return [d.ebolaCases count];
    else
    {
        //show "strange" error message
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *d = [UIApplication sharedApplication].delegate;
    NSString *identifier=[NSString stringWithFormat:@"%@%d",@"CarePodCell",(int)indexPath.row];
    [tableView registerNib:[UINib nibWithNibName:@"IndividualCasesCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:identifier];
    IndividualCasesCell *cell = (IndividualCasesCell *)[tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        
    } else
    {
        UILabel *nameLbl = (UILabel*)[cell.contentView viewWithTag:1];
        EbolaCase *ebolaCase = [d.ebolaCases objectAtIndex:[d.ebolaCases count] - indexPath.row - 1];
        nameLbl.text = ebolaCase.name;
        
        UILabel *dateLbl = (UILabel*)[cell.contentView viewWithTag:2];
        dateLbl.text = [NSDateFormatter localizedStringFromDate:ebolaCase.date
                                                      dateStyle:NSDateFormatterShortStyle
                                                      timeStyle:NSDateFormatterNoStyle];
    }
    
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
    //cell.textLabel.text = @"My Text";
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *d = [UIApplication sharedApplication].delegate;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    CasesDetailViewController *cDetails = [[CasesDetailViewController alloc]init];
    cDetails = (CasesDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"CasesDetailViewController"];
    cDetails.thisCase = [d.ebolaCases objectAtIndex:[d.ebolaCases count] - indexPath.row - 1];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:cDetails animated:YES completion:nil];
    });
    
    //[self presentViewController:cDetails animated:YES completion:^{}];
}

@end

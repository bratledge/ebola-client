//
//  AppDelegate.h
//  Ebola
//
//  Created by Administrator on 8/14/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSDictionary *coordinates;

@property (strong, nonatomic) NSMutableArray *ebolaCases;
@property (strong, nonatomic) NSMutableArray *styles;

@property (strong, nonatomic) NSMutableArray *dailyDates;
@property (strong, nonatomic) NSMutableArray *dailyCases;

@property (strong, nonatomic) NSString *suspectedConfirmedCases;
@property (strong, nonatomic) NSString *suspectedCaseDeaths;
@property (strong, nonatomic) NSString *confirmedLabCases;

@property (strong, nonatomic) NSString *customMessage;
@property (strong, nonatomic) NSString *vaccineMessage;
@property (strong, nonatomic) NSString *vaccineColor;
@property (strong, nonatomic) NSString *latestCases;
@end

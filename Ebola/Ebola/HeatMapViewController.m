//
//  HeatMapViewController.m
//  Ebola
//
//  Created by Administrator on 8/26/14.
//  Copyright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import "HeatMapViewController.h"
#import "EbolaCase.h"
#import "NYSliderPopover.h"

@interface HeatMapViewController ()

@property (nonatomic, strong) IBOutlet NYSliderPopover *slider;
- (IBAction)sliderValueChanged:(id)sender;

@end

@implementation HeatMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.mapView.delegate = self;
    AppDelegate *d = [[UIApplication sharedApplication] delegate];
    hm = [[HeatMap alloc] initWithData:d.coordinates];
    [self.mapView addOverlay:hm];
    [self.mapView setVisibleMapRect:[hm boundingMapRect] animated:YES];
    self.mapView.showsUserLocation = YES;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSString *theDate = [dateFormat stringFromDate:now];
    self.date.text = theDate;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    return [[HeatMapView alloc] initWithOverlay:overlay];
}


- (void)updateMapWithNewPoints:(NSDate *)date {
    AppDelegate *d = [[UIApplication sharedApplication] delegate];
    NSMutableDictionary *placeMarkLocations = [[NSMutableDictionary alloc] initWithCapacity:5000];
    
    for (EbolaCase *e in d.ebolaCases)
    {
        MKMapPoint mapPoint = MKMapPointForCoordinate(
                                                      CLLocationCoordinate2DMake(e.latitude, e.longitude));
        
        NSValue *pointValue = [NSValue value:&mapPoint withObjCType:@encode(MKMapPoint)];
        
        if (date)
        {
            if ([e.date compare:date] == NSOrderedAscending) {
                NSLog(@"date1 is later than date2");
                [placeMarkLocations setObject:[NSNumber numberWithInt:1] forKey:pointValue];
            }
        }
    }
    [self.mapView removeOverlay:hm];
    MKMapRect r = [hm boundingMapRect];
    hm = [[HeatMap alloc] initWithData:placeMarkLocations];
    [self.mapView addOverlay:hm];
    [self.mapView setVisibleMapRect:r animated:YES];
    
}

- (IBAction)sliderValueChanged:(id)sender
{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    [comps setMonth:1];
    [comps setYear:2013];
    NSDate *firstYear = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    int timestampFirstCase = [firstYear timeIntervalSince1970];
    int timestampNow = [[NSDate date] timeIntervalSince1970];
    
    
    int sliderTime = timestampFirstCase + self.slider.value * ((timestampNow-timestampFirstCase) * 0.01);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/yy"];
    
    NSString *stringFromDate = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:sliderTime]];
    
    self.slider.popover.textLabel.text = [NSString stringWithFormat:@"%@", stringFromDate];
    
    [self updateMapWithNewPoints:[NSDate dateWithTimeIntervalSince1970:sliderTime]];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    NSString *theDate = [dateFormat stringFromDate:[NSDate dateWithTimeIntervalSince1970:sliderTime]];
    self.date.text = theDate;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

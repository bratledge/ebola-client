//
//  main.m
//  Ebola
//
//  Created by Administrator on 8/14/14.
//  CopyZright (c) 2014 Bryan Ratledge. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
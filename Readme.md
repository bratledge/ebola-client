# Ebola Tracker
> Track Ebola Virus (iOS)

Ebola Client was designed to display all known Ebola cases in mobile format in map and list format and display growth rates and relevant news links to inform anyone of the status of Ebola worldwide.

![](preview.png)

## Installation

OS X -> iOS:

```sh
install latest OSX
install latest xcode
```

## Release History

* 1.3.1
    * FIX: Progress on chart, added vaccine status, fixed URL issues
* 1.0.1
    * Added date slider on heat map
* 1.0
    * Initial release

## Meta

Bryan Ratledge – [@YourTwitter](https://twitter.com/dbader_org) – YourEmail@example.com

Distributed under the MIT license.

[https://bitbucket.org/bratledge/ebola-client/](https://bitbucket.org/bratledge/ebola-client/)

## Contributing
